from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.models import User
from .forms import UserForm

def usrDel(request, pk=None):

    if not request.user.is_staff:
        raise PermissionDenied

    if pk:
        usr = User.objects.get(pk=pk)
        usr.delete()

    return HttpResponseRedirect("/user")


def usrView(request, pk=None):

    if not request.user.is_staff:
        raise PermissionDenied

    usr = User.objects.all()

    if request.method == "GET":
        forms = []
        for u in usr:
            forms.append (UserForm(instance=u))
        return render (request, "userList.html", {"forms": forms})

    if request.method == "POST" and pk:
        form = UserForm(request.POST)
        usr = User.objects.get(pk=pk)
        if form.is_valid ():
            usr.is_staff = form.cleaned_data["is_staff"]
            usr.save()

            return HttpResponseRedirect("/user")
