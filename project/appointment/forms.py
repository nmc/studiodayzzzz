from django.contrib.admin import widgets
from django.forms import ModelForm, SplitDateTimeField, TextInput
from django.forms.widgets import SplitDateTimeWidget
from .models import Reservation, Slot

class SlotForm (ModelForm):

    class Meta:
        model = Slot
        fields = ["start", "end"]
        widgets = { 'start': TextInput ( attrs={ 'type': 'datetime-local'
                                               , 'onchange': 'adjust_slot_end(event)'
                                               })
                  , 'end': TextInput (attrs={'type': 'datetime-local'})
                  }

class ReservationForm (ModelForm):

    class Meta:
        model = Reservation
        fields = ["slot", "notes", "department"]
