from django.urls import path
from django.shortcuts import render
from .views import ( calView, meinresView, freeSlots, resView, sloView
                   , adminSlotDelete, adminSlotList
                   )

urlpatterns = [ path ("slot-list", adminSlotList, name="slot-list")
              , path ("slot", sloView, name="slot-create")
              , path ("slot/<int:pk>", sloView, name="slot-update")
              , path ("slot/<int:pk>/delete", adminSlotDelete, name="slot-delete")
              , path ("free-slots", freeSlots, name="free-slots")
              , path ("meine-reservierung", meinresView, name="meires")
              , path ("meine-reservierung/<int:pk>", meinresView, name="meires-detail")
              , path ("reservation", resView, name="reservation-index")
              , path ("reservation/<int:pk>", resView, name="reservation-detail")
              ]
