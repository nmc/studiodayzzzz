from django.template.loader import render_to_string
from calendar import HTMLCalendar, monthrange
from .models import Slot, Reservation

class EventCalendar(HTMLCalendar):

    def __init__(self, slots=None):
        super(EventCalendar, self).__init__()
        self.slots = slots

    def formatday(self, day, weekday, slots):
        """
        Return a day as a table cell.
        """
        slots_of_day = [s.__dict__ for s in slots.filter (start__day=day)]
        reserved = False
        dayclass = []

        if len(slots_of_day) > 0:
            dayclass = dayclass + ["day-with-slot"]

        for s in slots_of_day:
            try:
                res = Reservation.objects.get(slot=s['id'])
            except:
                s["res"] = None
            else:
                dayclass = dayclass + ["day-with-reservation"]
                s["res"] = res.__dict__

        dayDialog = render_to_string("dayDialog.html", {"dates": slots_of_day})

        if day == 0:
            return "<td class='noday'>{}</td>".format(dayDialog)
        else:
            dayclass= dayclass + [self.cssclasses[weekday]]
            if len(slots_of_day) == 0:
                return "<td class='{}'>{}</td>".format(' '.join(str(w) for w in dayclass), day)
            else:
                return "<td class='{}' onclick='dayclick(event)'>{}{}</td>".format(' '.join(str(w) for w in dayclass), day, dayDialog)

    def formatdaydialog(self, day, weekday, slots):
        slots_html = "<ul>"
        for slot in slots_from_day:
            slots_html += slot.notes + "<br>"
        slots_html += "</ul>"

    def formatweek(self, theweek, slots):
        """
        Return a complete week as a table row.
        """
        s = ''.join(self.formatday(d, wd, slots) for (d, wd) in theweek)
        return '<tr>%s</tr>' % s

    def formatmonth(self, theyear, themonth, withyear=False):
        """
        Return a formatted month as a table.
        """
        slots = Slot.objects.filter(start__month=themonth)
        v = []
        a = v.append
        a('<table border="0" cellpadding="0" cellspacing="0" class="month">')
        a('\n')
        a(self.formatmonthname(theyear, themonth, withyear=withyear))
        a('\n')
        a(self.formatweekheader())
        a('\n')
        for week in self.monthdays2calendar(theyear, themonth):
            a(self.formatweek(week, slots))
            a('\n')
        a('</table>')
        a('\n')
        return ''.join(v)

    def formatOverview(self):
        """
        Return formatted slots, from today to end of month one year later.
        """



