from django.db import models
from django.contrib.auth.models import User

class Slot (models.Model):

    class Meta:
        ordering = ["start"]

    start = models.DateTimeField()
    end = models.DateTimeField()

    def __str__(self):
        """To string method."""
        return str(self.start)

class Reservation (models.Model):
    user = models.ForeignKey    ( User
                                , on_delete=models.CASCADE
                                , default=1
                                , related_name="reservation"
                                )
    slot = models.OneToOneField ( Slot
                                , on_delete=models.CASCADE
                                , null=True
                                , related_name="reservation"
                                )
    notes = models.TextField    ( null=True
                                , blank=True
                                )
    confirmed = models.BooleanField ( default = False )
    department = models.CharField (max_length=20, blank=True)
