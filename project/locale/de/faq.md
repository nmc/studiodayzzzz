# Welche Inhalte filmen wir?

Die Studio Days sind vorgesehen für universitäre Themen innerhalb von Wissenschaft, Lehre und Forschung. Bei einer kommerziellen oder privatwirtschaftlichen Förderung bedarf es einer Offenlegung.

# Wie sieht es aus mit dem Copyright von Bildern und Grafiken?

Das Studio Days-Angebot enthält keine Überprüfung Ihrer Inhalte auf
Copyright-Verletzungen. Bitte klären Sie deshalb vorab die Rechte zu den
Bildern und Grafiken in Ihren Slides ab. Verwenden Sie nur Bilder und Grafiken,
zu denen Sie die Rechte haben. Am sichersten ist es, wenn Sie das Bild oder die
Grafik **selbst gemacht** haben oder **lizenzfreie Bilder** verwenden, wie z.B.
von Pixabay, Unsplash, Pexels etc. Mehr Informationen zu Bildrechten an der
Universität Basel finden Sie unter [Bildrechte & Bildunterschriften (deutsch)](https://www.unibas.ch/de/Universitaet/Administration-Services/Bereich-Rektorin/Kommunikation-Marketing/Web-Services/Web-Desk/Bildrechte-Bildunterschriften.html){: target='_blank'}, [Online Image Rights & Photo Credits (englisch)](https://www.unibas.ch/en/University/Administration-Services/The-President-s-Office/Communications-Marketing/Web-Services/Web-Desk/Online-Image-Rights_Photo-Credits.html){: target='_blank'}, unter [Intellectual Property Rights](https://researchdata.unibas.ch/en/legal-issues/intellectual-property-rights/){: target='_blank'} und im Merkblatt [Datenschutz & Urheberrecht](https://www.unibas.ch/dam/jcr:4d0daf2a-1980-4e36-a939-64f8da58cc0d/Merkblatt_Datenschutz_Urheberrecht_20200313.pdf){: target='_blank'}.

Das New Media Center übernimmt keine Haftung für Copyright-Verletzungen. Wir verweisen Sie auf den obigen rechtlichen Rahmen, in dem Sie sich in der Universität bewegen und gehen davon aus, dass Sie diesen zur Kenntnis genommen haben und sich dementsprechend verhalten.

# Kann ich das Video auch selbst schneiden?

Selbstverständlich! Falls Sie keinen Schnitt wünschen, stellen wir Ihnen gerne das Rohmaterial zur Verfügung. Bringen Sie zum Termin einfach eine externe Festplatte mit ausreichend Speicherplatz mit (ca. 100-200 GB).

# Kann ich auch mehrere Kameras buchen?

Leider nein; unser Team dreht an den Studio Days nur mit einer Kamera.

# Kann ich mehrere Zeitfenster hintereinander buchen und länger aufnehmen?

Nein, wir können nur ein Zeitfenster pro Video anbieten.

# Könnt ihr mein Video auch hosten?

Leider können wir keine Videos hosten.

# Was kostet der Studio Days Service?

Der Service wird als Pilot gestartet. Er ist für Angehörige der Universität Basel zur Zeit gratis.

