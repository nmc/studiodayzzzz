Would you like a professionally produced presentation video?  
Then sign up for our Studio Days!
{ .teaser }

As a **member of the University of Basel,** you can book a **30-minute time
slot** at the New Media Center studio, and together we’ll produce a short,
simple video. You bring along the content and performance; we’ll provide the
technology and editing!

![Eine Stopuhr](/static/images/01_aufnahmezeit.svg "30 minutes recording time")
{ .illustrated-offer }

![Generische Darstellung eines Videos](/static/images/02_video.svg "A 5- to 10-minute final video")
{ .illustrated-offer }

![Generische Darstellung zweier Slides](/static/images/03_slides.svg "Your own slides in the video")
{ .illustrated-offer }

![Eine Filmklappe](/static/images/04_professionell.svg "Professional camera, sound and editing")
{ .illustrated-offer }

# How does it all work?

You’ll be standing in front of a colour background in a professionally lit set.
You’ll see your submitted slides during the recording. After the recording,
we’ll do the editing and send you the finished video as an MP4 file. Book an
appointment [here](/appointment/free-slots)!
