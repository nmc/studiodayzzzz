# How does a Studio Days session work?

1. You book an appointment via our [Webtool](/appointment/free-slots).
2. Each appointment has a maximum length of 30 minutes.
3. You’ll arrive at the studio, we’ll briefly explain the set-up, and then
   we’ll start recording.
4. We have one small request: please arrive at the studio **10 minutes before
   your time slot** so that we have enough time for you and can make sure that
   everything goes smoothly.
5. Please note that your submitted slides should ideally be the final version.
   During the recording, we’ll only have limited time to change the slides.
6. Towards the end of the 30 minutes, we’ll let you know that time is up.
7. We’ll deliver the final edited video to you **within one to two weeks**.

# How long will my video be?

The final videos are no more than **5 to 10 minutes long**. The 30 minutes of
filming in the studio **does not** correspond to a 30-minute presentation. We
recommend that you plan short and snappy presentations with only a few slides –
not whole lectures.

# How should I send in my slides?

At least 2 days before the recording, please send your slides as both **a PDF
and a PowerPoint file** (not as Pages or Keynote) to our email address:
**contact-nmc@unibas.ch.**

If you have videos or animations embedded in the presentation, please send
these to us separately. Please don’t add any transitions between the slides, as
we cannot include these in the edit.

[Here is a PowerPoint
template](/static/downloadables/StudioDays_PowerPoint_Template_EN.pptx) that
you are welcome to use.

In the studio, you will only see the slides, not the presenter mode. We
therefore recommend that you practise your text beforehand.

**Tip:** legible slides are essential. The text shouldn’t be too small and the
slides shouldn’t be crammed full with information. Keep slides as clean and
simple as possible.

# How should I prepare?

We recommend that you practise your text in advance so that you can speak
confidently and off by heart in front of the camera. It’ll also give you a feel
for the duration of the video. For more tips on writing your text and slides,
see ["Writing for
Multimedia"](https://tales.nmc.unibas.ch/de/writing-for-multimedia-45/){:target="_blank"}.
