{
  description = "StudiodayzZzZ flake";
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-21.11;
  };
  outputs = { self, nixpkgs }:
  let
    pkgs = import nixpkgs {
        inherit system;
        overlays = [];
    };
    tuntapName = "tapNmcWeb";
    pythonPackages = pkgs.python3Packages;
    system = "x86_64-linux";
    qemuAdr = "10.13.12.28";
    qemuConfig = { pkgs, config, lib, ... }: {
      imports = [
        "${nixpkgs}/nixos/modules/virtualisation/qemu-vm.nix"
      ];
      # virtualisation.diskImage = "${target}.qcow2";
      # virtualisation.qemu.options = ["-drive file=${qcow2Drive}"];
      environment.systemPackages = with pkgs; [
        docker-compose
        vim
      ];
      networking.defaultGateway = {
        # address = "10.0.2.1";
        address = "10.13.12.1";
        interface = "eth0";
      };
      networking.firewall.enable = false;
      networking.nameservers = [ "131.152.1.5" "208.67.222.222" ];
      networking.useDHCP = false;
      users.users.nmc-schnauz = {
        isNormalUser = true;
        createHome = true;
        home = "/home/nmc-schnauz";
        password = "bling";
        uid = 1000;
        extraGroups = [ "wheel" "docker" ];
        shell = pkgs.zsh;
      };
      security.sudo = {
        enable = true;
        wheelNeedsPassword = false;
      };
      services.sshd.enable = true;
      virtualisation.msize = 8192;
      virtualisation.memorySize = 8192;
      virtualisation.diskSize =  32768;
      virtualisation.docker.enable = true;
      virtualisation.qemu = {
        networkingOptions = [
          "-nic tap,ifname=${tuntapName},script=no,downscript=no,model=virtio-net-pci"
        ];
        options = [
          "-display none"
          # "-nographic"
        ];
      };
    };
  in rec {
    qemu = import "${nixpkgs}/nixos" {
      inherit system;
      configuration = {...}: {
        imports = [qemuConfig];
        networking.interfaces = {
          eth0.useDHCP = false;
          eth0.ipv4.addresses = [{
            address = qemuAdr;
            prefixLength = 24;
          }];
        };
      };
    };
    defaultPackage.x86_64-linux = qemu.config.system.build.vm;
    devShell.x86_64-linux = pkgs.mkShell {
      buildInputs = [
        pkgs.qemu
        (pkgs.writeShellScriptBin "setupNetworkLink" ''
          ip tuntap add mode tap user cri name ${tuntapName}
          ip link set ${tuntapName} up
          ip link set ${tuntapName} master br0
        '')
        (pkgs.writeShellScriptBin "cpDjangoToQemu" ''
          rsync -avu ./django/ nmc-schnauz@${qemuAdr}:~/django/
        '')
        (pkgs.writeShellScriptBin "cpAllToQemu" ''
          rsync -avu --exclude 'nixos.qcow2' ./ nmc-schnauz@${qemuAdr}:~/
        '')
        (pkgs.writeShellScriptBin "buildQemu" ''
          nix build .#qemu.config.system.build.vm
        '')
        pkgs.python3
        pkgs.python3Packages.pip
        pkgs.nodePackages.npm
        pkgs.nodejs

      ];
      shellHook = ''
        export PS1='\u@nmc-studiodayzZzZ \$ '
        export PIP_PREFIX=$(pwd)/_build/pip_packages
        export PYTHONPATH="$PIP_PREFIX/${pkgs.python3.sitePackages}:$PYTHONPATH"
        export PATH="$PIP_PREFIX/bin:$PATH"
        unset SOURCE_DATE_EPOCH
      '';
    };
  };
}
