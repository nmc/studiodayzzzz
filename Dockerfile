# Use an official Python runtime as a parent image
FROM python:3.10-slim-bullseye
EXPOSE 8000
LABEL maintainer="contact-nmc@unibas.ch"
ARG DJANGO_SETTINGS_MODULE
ENV DJANGO_SETTINGS_MODULE=${DJANGO_SETTINGS_MODULE}

# prepare log folder
# Create source code environment
RUN mkdir -p /opt/src
WORKDIR /opt/src


ADD static /opt/static
ADD requirements ./requirements
ADD project ./project
WORKDIR /opt/src/project

RUN pip install --upgrade pip
RUN pip install -r ../requirements/${DJANGO_SETTINGS_MODULE}.txt
RUN mkdir /opt/static-volume

CMD bash -c "cp -r /opt/static/* /opt/static-volume && python manage.py runserver 0.0.0.0:8000 --settings=config.settings.${DJANGO_SETTINGS_MODULE}"
